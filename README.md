# Tiller Helm chart

* Creates tiller for helm.

```bash
helm package .
helm template tiller-0.1.0.tgz | kubectl -f -
```
